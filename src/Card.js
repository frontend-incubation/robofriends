import React from 'react';

const Card = props => {
  return (
    <div className="card bg-light-green dib br3 pa3 ma2 grow shadow-5 tc">
      <img
        src={`https://robohash.org/${props.username}?200x200`}
        alt="robots"
      />
      <div>
        <h2>{props.name}</h2>
        <p>{props.email}</p>
      </div>
    </div>
  );
};

export default Card;
